extends Node
@onready var text_edit = $"../Camera3D/TextEdit"

var socket = WebSocketPeer.new()
var url
var message
var message_type

# do not process until connected
func _ready():
	set_process(false) 

# connects and processes
func init(new_url):
	url = new_url
	socket.connect_to_url(url)
	set_process(true) 

# Sends any messages waiting to be sent
func handle_message():
	if message != null:
		if message_type == TYPE_STRING:
			socket.send_text(message)
		else:
			socket.send(message, WebSocketPeer.WRITE_MODE_BINARY)
		message = null

# Readys a message to be sent
# based on type, will send as string or byte array only
# (May need a buffer in the future?)
func send_message(new_message, new_message_type):
	message = new_message
	message_type = new_message_type

# Deal with all incoming packets
func handle_all_packets():
	while socket.get_available_packet_count():
		var current_packet = socket.get_packet()
		
		# ASSUMES UTF8 unless nonstring!
		if socket.was_string_packet():
			print(current_packet.get_string_from_utf8())
			text_edit.text = str(text_edit.text, url, ":  ", current_packet.get_string_from_utf8(), "\n")
		else:
			print("Raw Packet: ", current_packet)

# Close the socket when done
func close():
	socket.close()

# Runs every processing frame
func _process(_delta):
	socket.poll()
	var state = socket.get_ready_state()
	if state == WebSocketPeer.STATE_OPEN:
		handle_all_packets()
		handle_message()
	elif state == WebSocketPeer.STATE_CLOSING:
		pass
	elif state == WebSocketPeer.STATE_CLOSED:
		var code = socket.get_close_code()
		var reason = socket.get_close_reason()
		print("WebSocket closed with code: %d, reason %s. Clean: %s" % [code, reason, code != -1])
		set_process(false) 
