extends Node

@onready var message_button = $"../Camera3D/message-button"
@onready var connect_button = $"../Camera3D/connect-button"
@onready var websocket_client = $"../websocket-client"
@onready var ip = $"../ip"


func _ready():
	print("ready.")

func _on_messagebutton_pressed():
	websocket_client.send_message("Boop!", TYPE_STRING)

func _on_connectbutton_pressed():
	websocket_client.init(ip.text)
	message_button.disabled = false
	
